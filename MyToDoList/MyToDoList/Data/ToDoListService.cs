﻿using MyToDoList.Common.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyToDoList.Data
{
    public class ToDoListService: ISingletonInterface
    {
        List<ToDoModel> todos=new List<ToDoModel>();

        public ToDoListService() { }

        public async Task<List<ToDoModel>> GetAllAsync()
        {
            return todos;
        }

        public async Task AddAsync(ToDoModel model)
        {
            if(model != null)
            {
                todos.Add(new ToDoModel() { 
                    Id=Guid.NewGuid(),
                    Content=model.Content,
                    NotifyTime= model.NotifyTime,
                });
            }
        }

        public async Task UpdateAsync(ToDoModel model)
        {
            var old = todos.FirstOrDefault(x => x.Id == model.Id);
            old.NotifyTime = model.NotifyTime;
            old.Content = model.Content;

        }

        public async Task<ToDoModel> GetAsync(Guid id)
        {
            var model= todos.FirstOrDefault(x => x.Id == id);
            if(model != null)
            {
                return new ToDoModel() { 
                    Id = model.Id,
                    Content = model.Content,
                    NotifyTime= model.NotifyTime,
                };
            }
            return null;
        }
    }
}
