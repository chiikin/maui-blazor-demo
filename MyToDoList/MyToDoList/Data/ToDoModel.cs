﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyToDoList.Data
{
    public class ToDoModel
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime NotifyTime { get; set; }
    }
}
