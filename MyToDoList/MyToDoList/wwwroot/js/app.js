﻿
function alertMsg(msg) {
    alert(msg);
}
/**
 * 
 * @param {any} dotNetHelper 注入的razor页面实例
 * @param {any} val1
 * @param {any} val2
 */
async function getRandomId(dotNetHelper, val1) {
    await dotNetHelper.invokeMethodAsync("SetRandomValueAsync", `${new Date().valueOf()}-${val1}`);
}